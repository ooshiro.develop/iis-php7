Docker IIS with PHP7
=======================

IISでPHP7開発を行うためのテンプレートプロジェクト
※Docker for Windowsのインストールが前提


## 初期構築  

### Hyper-V有効化(すでに有効な場合、作業不要)

PowerShellを管理者モードで起動  
ユーザアカウント制御ダイアログがでるので【はい】を押下
下記コマンドを実行
```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```
インストールが終わると再起動を求められるので再起動する

### Dockerインストール
下記からDockerをダウンロードする  
[Docker](https://docs.docker.com/docker-for-windows/install/)  
インストールの際、ユーザアカウント制御ダイアログがでるので【はい】を押下  

#### インストール後確認  
`docker version` でバージョン情報が表示されること(エラーメッセージが表示されないこと)  
`docker run hello-world` が動くこと

## 初期化実行

Windowsタスクバー上のDockerアイコンを右クリックし、`Switch to windows containers...`をクリックしコンテナの切り替えを行う

```
docker-compose up -d --build
```

### IISが参照するドキュメントルートを変更

```
powershell
Import-Module WebAdministration
cd iis:\
Set-ItemProperty "IIS:\Sites\Default Web Site" -name physicalPath -value C:\inetpub\webroot
```

---

## サーバ起動

Docker起動
```
docker-compose restart
```
下記に接続確認
```
http://localhost:880/info.php
```

---

## サーバ停止

```
docker-compose stop
```

---


#### 注意

`./src`配下をIIS公開フォルダとします
php.iniはほぼ生の状態なので、必要に応じて`extension`のコメントを解除すること
